---
concept: '0x33FD5c3a501B59d5Ca68D75EaD1F3351eaBA1b93'
name: Smart Contract Development
---

Smart Contracts are _programs_ that anyone can trustlessly interact with, run on
a global distributed system.


## References

- [ethereum](https://ethereum.org/)

