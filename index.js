const matter= require('gray-matter')
const fs = require('fs')
const ethers = require('ethers')
const fathom = require('fathom-contracts')
const ipfsAPI = require('ipfs-api');
const ipfs = ipfsAPI('ipfs.infura.io', '5001', {protocol: 'https'});

let mnemonic = process.env.SEED
let provider = ethers.getDefaultProvider(42)

let wallet = (new ethers.Wallet.fromMnemonic(mnemonic)).connect(provider)

let conceptDocs = fs.readdirSync('./concepts')

async function main () {
  for(var i in conceptDocs) {
    const file = fs.readFileSync('./concepts/' + conceptDocs[i] );
    let conceptDoc = matter(file)
    let address = conceptDoc.data.concept
    let Concept = new ethers.Contract(address, fathom.Concept.abi, wallet)

    let conceptData = await Concept.data()
    let conceptOwner = await Concept.owner()

    if(conceptOwner !== wallet.address) throw new Error('Concept is not owned by Concept Manager')

    let hash = (await ipfs.add([file], {pin:true}))[0].hash
    let hashBytes = '0x' + (Buffer.from(hash, 'utf8')).toString('hex')

    if(conceptData != hashBytes) {
      await Concept.changeData(hashBytes)
    }
  }
}

main()
